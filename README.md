# Chinese Agricultural Moon Lunar Calendar

This is a Calendar program written by C language on the DOS system two years ago. It shows the calendar of the lunar day, the Chinese climate, the Chinese moon phase, moon rise and fall, the south China tide situation, the zodiac, the Chinese lunar calendar and the chinese time count.The full screen display is that I use the screen to draw dot array bitmaps, so all the Chinese characters on the screen are not words, but the drawn dot array bitmaps. This program end of use is AD2040, and now it is AD2018.9.25. There is many function can be modification and promotion. Specifically, I have to being due to work. Now I share it to anyone can help improve the software. Originally I wanted to draw bitmap is Chinese paper-cut Zodiac and chinese Moon phase. Then make a lunar calendar, and also display the small AD date below the lunar calendar. 

这是我两年前写的一个在DOS系统下汉字显示中国月历，能显示当天公元历、中国气候、中国月相、月出月落，南中国潮汐情况、生肖、中国农历和时辰的小程序。全屏显示的是我通过屏幕描点以实现汉字显示，所以屏幕上所有汉字并不是字，而是绘图出来的图形。该程序使用的最大年份是 AD2039年，现在是AD2018.9.25 该程序还有需要修改和提升的空间。具体如何做，暂时由于工作原因，我没有完成。首先共享出来，看是否有人能够帮助提升该软件。关于屏幕显示底下空白处，我原本是想将中国十二生肖的剪纸图和当天月相。然后再做个农历的月历，在农历下方还显示小号AD日期。通过键盘或鼠标上下键可以翻页查看月份和日期，但我还没完成。

原设计完全版布局如链接所示
https://gitlab.com/freedom99/chinese-agricultural-moon-lunar-calendar/blob/master/Lunar_Calendar.jpg